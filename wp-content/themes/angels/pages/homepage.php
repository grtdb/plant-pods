<?php /* Template Name: Homepage */
get_header(); the_post(); $page_id = get_the_ID(); ?>

<div id="main" class="page-homepage">

	<?php if ( $slides = get_field( 'gallery_slides' ) ) { ?>
		<section id="home-slideshow">
			<div class="gallery" data-lazy_load="true" data-pagination="true" data-fadecontent=".holder">
				<div class="gallery-slider">
					<?php foreach ( $slides as $i => $slide ) { ?>
						<div class="gallery-slider-slide owl-lazy" data-src="<?= $slide[ 'image' ][ 'sizes' ][ 'homepage_slider' ]; ?>" data-index="<?= $i; ?>">
							<?php if ( $slide[ 'show_content' ] ) { ?>
								<div class="holder">
									<div class="container">
										<div class="content">
											<?php if ( $title = $slide[ 'title' ] ) { ?>
												<h1 class="title"><?= $title; ?></h1>
											<?php }
											if ( $text = $slide[ 'text' ] ) { ?>
												<div class="text">
													<?= $slide[ 'text' ]; ?>
												</div>
											<?php }
											if ( $slide[ 'show_button' ] ) { ?>
												<a class="button" href="<?= $slide[ 'button_href' ]; ?>"><?= $slide[ 'button_text' ]; ?></a>
											<?php } ?>
										</div>
									</div>
								</div>
							<?php } ?>
						</div>
					<?php } ?>
				</div>
			</div>
		</section>
	<?php } ?>

	<?php if ( !$title = get_field( 'homepage_title' ) ) {
			$title = get_the_title();
	}
	dbHelper::get_part( 'description', array( 'title' => $title ) ); ?>

	<?php dbHelper::get_part( 'category-carousel', array(
		'categories' => get_terms( array( 'taxonomy' => 'product_cat' ) ),
	) ); ?>

	<?php dbHelper::get_part( 'mobile-category', array(
		'categories' => get_terms( array( 'taxonomy' => 'product_cat' ) ),
	) ); ?>

</div>

<?php get_footer();