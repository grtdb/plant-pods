<?php /* Template Name: Stores */
get_header(); the_post(); $page_id = get_the_ID(); ?>

<div id="main" class="page-stores">

	<?php dbHelper::get_part( 'description' ); ?>

	<?php dbHelper::get_part( 'stores' ); ?>

</div>

<?php get_footer();