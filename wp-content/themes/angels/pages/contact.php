<?php /* Template Name: Contact */
get_header(); the_post(); $page_id = get_the_ID(); ?>

<div id="main" class="page-contact">

    <?php dbHelper::get_part( 'description' ); ?>

	<section>
		<div class="container">
			<div class="row">
				<div class="col-md-5">
					<div id="contact-details">
						<h2 class="sub-title">Contact Details</h2>
						<?php if ( $phone = get_option( 'contact_phone' ) ) { ?><a class="phone" href="<?= $phone; ?>"><span>T. </span><?= $phone; ?></a><?php } ?>
						<?php if ( $email = get_option( 'contact_email' ) ) { ?><a class="email" href="<?= $email; ?>"><span>E. </span><?= $email; ?></a><?php } ?>
					</div>
					<div id="contact-returns">
						<h2 class="sub-title">Returns</h2>
						<p class="returns">Please follow the instructions on your delivery note.<br />
						<?php if ( $returns_details = get_page_by_path( 'returns' ) ) { ?>For more info please <a href="<?= get_permalink( $returns_details ); ?>">click here</a>.<?php } ?></p>
					</div>
				</div>
				<div class="col-md-7">
					<form id="contact-form" method="post" class="clearfix es-form ajax-form">
						<h2 class="sub-title">Contact Form</h2>
						<?php global $db_process_form;
						if ( !empty( $db_process_form->errors ) ) { ?>
							<div id="notification" class="error">
								<?php foreach ( $db_process_form->errors as $error ) { ?>
									<p><?= $error; ?></p>
								<?php } ?>
							</div>
						<?php }

						if ( isset( $_GET['success'] ) ) { ?>
							<div id="notification" class="success"><p><?= get_option( 'contact_form_success' ); ?></p></div>
						<?php }

						$values = array();
						foreach ( array( 'name', 'number', 'company', 'email', 'message' ) as $field ) {
							$key = 'contact_' . $field;
							if ( !empty( $_POST[$key] ) && empty( $contact_success ) ) {
								$values[$key] = $_POST[$key];
							} else {
								$values[$key] = '';
							}
						} ?>
						<div class="form-group">
							<div class="row">
								<fieldset class="col-6">
									<input type="text" class="form-control" id="name" name="contact_name" placeholder="Name" />
								</fieldset>
								<fieldset class="col-6">
									<input type="email" class="form-control" id="email" name="contact_email" placeholder="Email" />
								</fieldset>
							</div>
						</div>
						<div class="form-group">
							<div class="row">
								<fieldset class="col-6">
									<input type="tel" class="form-control" id="phone" name="contact_phone" placeholder="Phone Number" />
								</fieldset>
								<fieldset class="col-6">
									<?php $enquiry_options = explode( ',', get_option( 'contact_form_enquiry_options' ) ); ?>
									<select class="form-control" id="enquiry-type" name="contact_enquiry_type">
										<option disabled selected>Enquiry Type</option>
										<?php foreach ( $enquiry_options as $enquiry_option ) { ?>
											<option value="<?= $enquiry_option; ?>"><?= $enquiry_option; ?></option>
										<?php } ?>
									</select>
								</fieldset>
							</div>
						</div>
						<div class="form-group">
							<div class="row">
								<fieldset class="col-12">
									<textarea class="form-control" id="enquiry" name="contact_message" placeholder="Your Enquiry" rows="3"></textarea>
								</fieldset>
							</div>
						</div>
						<input type="hidden" name="action" value="contact_form_submission" />
						<input type="hidden" name="contact_url" value="" />
						<button type="submit" class="button float-right">Submit</button>
					</form>
				</div>
			</div>
		</div>
	</section>

    <?php dbHelper::get_part( 'stores' ); ?>

</div>

<?php get_footer();