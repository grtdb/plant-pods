<?php /* Template Name: Woocommerce Template */
get_header(); the_post();

$page_id = get_the_ID();

$page_class = '';
if ( is_cart() ) { $page_class = ' page-cart'; }
if ( is_checkout() ) { $page_class = ' page-checkout'; }
if ( is_account_page() ) { $page_class = ' page-account'; } ?>

<div id="main" class="page-woocommerce<?php echo $page_class; ?> site-content">

	<?php if ( !is_checkout() ) {
		dbHelper::get_part( 'breadcrumbs' );
	}

	dbHelper::get_part( 'description', array( 'text' => false ) );

	dbHelper::get_part( 'woo_message' );

	$section_id = 'content';
	if ( is_cart() ) {
		$section_id = 'cart-contents';
	} ?>

	<section id="<?= $section_id; ?>">
		<div class="container">
			<?php the_content(); ?>
		</div>
	</section>

</div>

<?php get_footer();
