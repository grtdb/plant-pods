<?php /* Template Name: Hire */
get_header(); the_post(); $page_id = get_the_ID(); ?>

<div id="main" class="page-hire">

	<?php dbHelper::get_part( 'description' ); ?>

	<?php if ( $hire_slides = get_field( 'hire_slides' ) ) { ?>
		<section id="hire-slider">
			<div class="container">
				<div class="gallery" data-loop="true" data-lazy_load="true" data-fadecontent=".inner">
					<div class="gallery-slider">
						<?php foreach ( $hire_slides as $i => $slide ) {
							if ( $image = $slide[ 'image' ] ) { ?>
								<div class="gallery-slider-slide owl-lazy" data-index="<?= $i; ?>" data-src="<?= $image[ 'sizes' ][ 'hire_gallery_slider' ]; ?>">
									<div class="inner">
										<div class="content">
											<p class="title"><?= $slide[ 'title' ]; ?></p>
											<?php if ( $text = $slide[ 'text' ] ) { ?><p><?= $text; ?></p><?php } ?>
										</div>
									</div>
								</div>
							<?php }
						} ?>
					</div>
					<div class="gallery-prev"></div>
					<div class="gallery-carousel" data-items="6" data-items_tab="4" data-spacing="20">
						<?php foreach ( $hire_slides as $i => $slide ) {
							if ( $image = $slide[ 'image' ] ) { ?>
								<div class="gallery-carousel-slide owl-lazy" data-index="<?= $i; ?>" data-src="<?= $image[ 'sizes' ][ 'hire_gallery_carousel' ]; ?>"></div>
							<?php }
						} ?>
					</div>
					<div class="gallery-next"></div>
				</div>
			</div>
		</section>
	<?php } ?>

	<?php if ( $hire_desc = get_field( 'hire_description' ) ) { ?>
		<section id="hire-content" class="part-description">
			<div class="container">
				<div class="dyn-content">
					<?php echo $hire_desc; ?>
				</div>
			</div>
		</section>
	<?php } ?>

	<?php $downloads = get_field( 'downloads' );
	if ( $downloads ) { ?>
		<section id="downloads">
			<div class="container">
				<?php if ( $downloads_title = get_field( 'downloads_title' ) ) { ?>
					<h2 class="title"><?php echo $downloads_title; ?></h2>
				<?php } ?>
				<div class="row">
					<?php foreach ( $downloads as $download ) {
						if ( $file = $download[ 'file' ][ 'url' ] ) { ?>
							<div class="col-md-6">
								<a href="<?php echo $file; ?>" class="download" target="_blank">
									<div class="item">
										<?php if ( $image = $download[ 'icon' ][ 'url' ] ) { ?>
											<img class="icon" src="<?php echo $image; ?>" alt="<?php echo $download[ 'title' ]; ?>" />
										<?php } ?>
										<p class="title"><?php echo $download[ 'title' ]; ?></p>
										<p class="download">Download</p>
									</div>
								</a>
							</div>
						<?php }
					} ?>
				</div>
			</div>
		</section>
	<?php } ?>

</div>

<?php get_footer();