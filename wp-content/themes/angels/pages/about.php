<?php /* Template Name: About Us */
get_header(); the_post(); $page_id = get_the_ID(); ?>

<div id="main" class="page-about">

	<?php dbHelper::get_part( 'description' ); ?>

	<?php if ( $history_slides = get_field( 'history_slides' ) ) { ?>
		<section id="history-slider">
			<div class="container">
				<div class="gallery" data-fadecontent=".holder">
					<div class="gallery-prev"></div>
					<div class="gallery-slider">
						<?php foreach ( $history_slides as $i => $slide ) {
							if ( $image = $slide[ 'image' ] ) { ?>
								<div class="gallery-slider-slide" data-index="<?= $i; ?>">
									<div class="history-slide-bg" style="background-image:url( '<?= $image[ 'sizes' ][ 'default_gallery_slider' ]; ?>' );"></div>
									<div class="inner">
										<div class="holder">
											<div class="image">
												<img src="<?= $image[ 'sizes' ][ 'history_slide' ]; ?>" alt="<?= $slide[ 'title' ]; ?>" />
											</div>
											<div class="content">
												<div class="wrap">
													<p class="year"><?= $slide[ 'year' ]; ?></p>
													<p class="title"><?= $slide[ 'title' ]; ?></p>
													<p class="text"><?= $slide[ 'text' ]; ?></p>
												</div>
											</div>
										</div>
									</div>
								</div>
							<?php }
						} ?>
					</div>
					<div class="gallery-next"><?= $history_slides[1][ 'year' ]; ?></div>
				</div>
				<div id="history-slider-nav" class="d-none d-md-block">
					<ul>
						<?php foreach ( $history_slides as $i => $slide ) {
							if ( $image = $slide[ 'image' ] ) { ?>
								<li data-index="<?= $i; ?>"<?php if ( $i == 0 ) { ?> class="active"<?php } ?>><?= $slide[ 'year' ]; ?></li>
							<?php }
						} ?>
					</ul>
				</div>
			</div>
		</section>
	<?php } ?>

	<?php if ( $history_title = get_field( 'history_title' ) ) {
		dbHelper::get_part( 'description', array( 'title' => $history_title, 'text' => get_field( 'history_text' ) ) );
	} ?>

</div>

<?php get_footer();