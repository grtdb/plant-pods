<?php /* Store single template */
get_header(); the_post(); $page_id = get_the_ID(); ?>

<div id="main" class="single-store">

	<?php dbHelper::get_part( 'description' ); ?>

	<?php $slides = get_field( 'gallery_slides' );
	if ( $slides ) { ?>
		<section id="store-gallery">
			<div class="container">
				<?php dbHelper::get_part( 'gallery', array( 'slides' => $slides ) ); ?>
			</div>
		</section>
	<?php } ?>

	<section id="store-details">
		<div class="container">
			<div class="row">
				<div class="col-md-7">
					<div class="row">
						<div class="col-md-6">
							<h2 class="sub-title">Details</h2>
							<img id="store-logo" src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/colour-logo.png" alt="Angels Fancy Dress" />
							<?php if ( $address = get_field( 'store_address' ) ) { ?>
								<address>
									<?php echo $address; ?>
								</address>
							<?php }
							if ( $phone = get_field( 'telephone_number' ) ) { ?>
								<p>Tel: <a href="tel:<?php echo $phone; ?>"><?php echo $phone; ?></a> - This number is not for web enquiries</p>
							<?php }
							if ( $email = get_field( 'email_address' ) ) { ?>
								<p>Email: <a href="mailto:<?php echo $email; ?>"><?php echo $email; ?></a></p>
							<?php } ?>
						</div>
						<div class="col-md-6">
							<h2 class="sub-title">Opening Hours</h2>
							<?php if ( $openings = get_field( 'opening_hours' ) ) { ?>
								<table>
									<?php foreach( $openings as $opening ) { ?>
										<tr><th><?php echo $opening[ 'days' ]; ?></th><td><?php echo $opening[ 'times' ]; ?></td></tr>
									<?php } ?>
								</table>
							<?php } ?>
						</div>
					</div>
					<?php if ( $details = get_field( 'store_details' ) ) { ?>
						<p id="location-details"><?php echo $details; ?></p>
					<?php } ?>
				</div>
				<div class="col-md-5">
					<?php if ( $map_location = get_field( 'map_coordinates' ) ) { ?>
						<a href="https://maps.google.com/maps/place/<?php echo $map_location; ?>" target="_blank">
							<div id="store-map">
								<?php $script_url = 'http://maps.googleapis.com/maps/api/js';
								if ( $api_key = get_option( 'google_maps_api_key' ) ) { $script_url .= '?key=' . $api_key; } ?>
								<script src="<?php echo $script_url; ?>"></script>
								<script>
									jQuery( function( $ ) {
										function initialize_map() {
											var mapCanvas = document.getElementById( 'store-map' );
											var mapLocation = new google.maps.LatLng( <?php echo $map_location; ?> );
											var mapZoom = 18;

											if ( isMobile() ) { mapZoom -= 1; }

											var mapOptions = {
												scrollwheel: false,
												center: mapLocation,
												zoom: mapZoom,
												mapTypeId: google.maps.MapTypeId.ROADMAP,
												draggable: false,
												disableDefaultUI: true
											};
											var location_map = new google.maps.Map( mapCanvas, mapOptions );
											var location_icon = '<?php echo get_stylesheet_directory_uri(); ?>/assets/img/map-pin.png';

											marker = new google.maps.Marker( {
												position: mapLocation,
												map: location_map,
												icon: location_icon,
											} );
										}
										google.maps.event.addDomListener( window, 'load', initialize_map );
									} );
								</script>
							</div>
						</a>
						<a id="store-map-button" class="button alt p-bold" href="https://maps.google.com/maps/place/<?php echo $map_location; ?>" target="_blank">
							View in a larger map
						</a>
					<?php } ?>
				</div>
			</div>
		</div>
	</section>

</div>

<?php get_footer();