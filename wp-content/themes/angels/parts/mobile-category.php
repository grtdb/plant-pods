<?php if ( isset( $categories ) ) { ?>
    <section class="mobile-category d-block d-md-none">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <?php foreach ( $categories as $category ) {
                        if ( is_int( $category ) ) {
                            $category = get_term( $category );
                        }
						if ( !$image = wp_get_attachment_image_src( get_woocommerce_term_meta( $category->term_id, 'thumbnail_id' ), 'mobile_category' )[0] ) {
							$image = dbHelper::get_placeholder();
						} ?>
                        <a class="content" href="<?= get_term_link( $category ); ?>">
							<div class="image" style="background-image:url( '<?= $image; ?>' );"></div>
							<?php if ( $title = $category->name ) { ?>
								<p class="title"><?= $title; ?></p>
							<?php } ?>
                        </a>
                    <?php } ?>
                </div>
            </div>
        </div>
    </section>
<?php }

