<?php if ( !isset( $title ) ) {
	$title = get_the_title();
}
if ( !isset( $text ) ) {
	$text = get_the_content();
} ?>
<section class="part-description">
	<div class="container">
		<h1 class="title"><?php echo $title; ?></h1>
		<?php if ( $text ) { ?>
			<div class="dyn-content">
				<?php echo $text; ?>
			</div>
		<?php } ?>
	</div>
</section>