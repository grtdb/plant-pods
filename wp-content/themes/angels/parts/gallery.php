<?php if ( $slides ) { ?>
	<div class="gallery" data-lazy_load="true" data-loop="true" data-fadecontent=".content">
		<div class="gallery-prev"></div>
		<div class="gallery-slider">
			<?php foreach ( $slides as $slide ) {
				if ( $image = $slide[ 'image' ] ) { ?>
					<div class="gallery-slider-slide" style="background-image:url( '<?php echo $image[ 'sizes' ][ 'default_gallery_slider' ]; ?>' );">
						<?php if ( $slide[ 'show_content' ] ) { ?>
							<div class="content">
								<?php if ( $title = $slide[ 'title' ] ) { ?><p class="title"><?php echo $title; ?></p><?php }
								if ( $text = $slide[ 'text' ] ) { ?><p><?php echo $text; ?></p><?php } ?>
							</div>
						<?php } ?>
					</div>
				<?php }
			} ?>
		</div>
		<div class="gallery-next"></div>
	</div>
<?php }