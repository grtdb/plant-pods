<?php if ( isset( $items ) ) { ?>
	<section <?php if ( isset( $id ) ) { ?>id="<?= $id; ?>" <?php } ?>class="product-carousel product-list">
		<div class="container">
			<?php if ( isset( $title ) ) { ?>
				<p class="title"><?= $title; ?></p>
			<?php } ?>
			<div class="gallery" data-loop="true">
				<ul class="gallery-carousel" data-items="5" data-spacing="30">
					<?php foreach ( $items as $item ) {
						if ( is_int( $item ) ) {
							$item = get_product( $item );
						}
						if ( !$image = dbHelper::get_featured_url( $item->post->ID, 'product_carousel' ) ) {
							$image = dbHelper::get_placeholder();
						}
						$title = $item->post->post_title; ?>
						<li class="gallery-carousel-slide product">
							<a href="<?= get_permalink( $item->post ); ?>">
								<div class="image">
									<img src="<?= $image; ?>" alt="<?= $title; ?>" />
								</div>
								<div class="content">
									<h3><?= $title; ?></h3>
									<?= $item->get_price_html(); ?>
								</div>
							</a>
						</li>
					<?php } ?>
				</ul>
			</div>
		</div>
	</section>
<?php }