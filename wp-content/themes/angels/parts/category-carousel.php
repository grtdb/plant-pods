<?php if ( isset( $categories ) ) { ?>
	<section class="category-carousel product-list d-none d-sm-block">
		<div class="container">
			<div class="gallery" data-loop="true">
				<div class="gallery-prev"></div>
				<ul class="gallery-carousel" data-items="4" data-items_tab="3" data-spacing="30">
					<?php foreach ( $categories as $category ) {
						if ( is_int( $category ) ) {
							$category = get_term( $category );
						}
						if ( !$image = wp_get_attachment_image_src( get_woocommerce_term_meta( $category->term_id, 'thumbnail_id' ), 'category_carousel' )[0] ) {
							$image = dbHelper::get_placeholder();
						}
						$title = $category->name; ?>
						<li class="gallery-carousel-slide product-category product">
							<a href="<?= get_term_link( $category ); ?>">
								<div class="image">
									<img src="<?= $image; ?>" alt="<?= $title; ?>" />
								</div>
								<div class="content">
									<p class="title"><?= $title; ?></p>
									<?php /*if ( $text = $category->description ) { ?><p><?= dbHelper::shorten_text( $text, 65 ); ?></p><?php }*/ ?>
								</div>
							</a>
						</li>
					<?php } ?>
				</ul>
				<div class="gallery-next"></div>
			</div>
		</div>
	</section>
<?php }