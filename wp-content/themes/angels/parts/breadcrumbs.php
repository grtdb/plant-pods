<section id="part-breadcrumbs">
	<div class="container">
		<div class="wrap">
			<p>You are here</p>
			<span>&gt;</span>
			<?php if ( !is_shop() ) { ?>
				<a href="/shop">Shop Online</a>
				<span>&gt;</span>
			<?php }
			do_action( 'db_location_breadcrumbs' ); ?>
		</div>
	</div>
</section>