<form role="search" method="get" class="product-search" action="<?= get_home_url(); ?>">
	<input type="search" class="search" placeholder="Search Products…" value="" name="s">
	<button><span class="fa fa-search"></span></button>
	<input type="hidden" name="post_type" value="product">
</form>