<div id="site-navigation-mob">
	<div id="site-navigation-mob-contact">
		<a href="tel:<?php echo get_option( 'contact_phone' ); ?>">Call Us: <?php echo get_option( 'contact_phone' ); ?></a>
	</div>
	<div class="mobile-search">
		<?php dbHelper::get_part( 'product-search' ); ?>
	</div>
	<?php wp_nav_menu( array( 'theme_location' => 'cat_menu', 'container' => false ) ); ?>
	<?php wp_nav_menu( array( 'theme_location' => 'menu', 'container' => false ) ); ?>
</div>