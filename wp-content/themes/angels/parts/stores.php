<section id="stores-list" class="part-display">
    <div class="container">
        <?php $stores = get_posts( array( 'post_type' => 'stores' ) );
		if ( $stores ) { ?>
			<div class="row">
				<?php foreach ( $stores as $store ) {
					$image = dbHelper::get_featured_url( $store->ID, 'store_thumbnail' );
					if ( !$image ) { $image = dbHelper::get_placeholder(); } ?>
					<div class="col-md-6">
						<a class="item" href="<?php echo get_permalink( $store ); ?>">
							<div class="background" style="background-image:url('<?php echo $image; ?>');"></div>
							<div class="holder">
								<div class="content">
									<p class="title"><?php echo $store->post_title; ?></p>
									<p class="p-bold details">Details</p>
								</div>
							</div>
						</a>
					</div>
				<?php } ?>
			</div>
		<?php } else { ?>
			<p class="empty">No Stores exist</p>
		<?php } ?>
    </div>
</section>