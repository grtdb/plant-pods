<?php /* 404 Page Template */
get_header(); the_post(); $page_id = get_the_ID(); ?>

<div id="main" class="page-404">

	<?php $title = '404 Error page not found!';
	$text = '<p>Sorry, the page you were looking for could not be found.</p>';
	dbHelper::get_part( 'description', array( 'title' => $title, 'text' => $text ) ); ?>

	<section>
		<div class="container">
			<div id="list-404" class="dyn-content">
				<p>Here are some links that may be useful:</p>
				<ul>
					<li><a href="/">Return to the homepage</a></li>
					<li><a href="/stores">View our highstreet stores</a></li>
					<li><a href="/shop">Browse our shop</a></li>
					<li><a href="/hire">View our costume hire information</a></li>
					<li><a href="/news">Look at out latest news</a></li>
					<li><a href="/about-us">Read about us</a></li>
					<li><a href="/contact">Get in contact with us!</a></li>
				</ul>
			</div>
			<div id="search-404">
				<p>Search for a product</p>
				<?php dbHelper::get_part( 'product-search' ); ?>
			</div>
		</div>
	</section>

</div>

<?php get_footer();