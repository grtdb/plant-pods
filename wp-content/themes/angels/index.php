<?php /* Blog Page */
get_header(); $page_id = get_option( 'page_for_posts' ); $page = get_post( $page_id );
$queried_obj = get_queried_object();
$on_term = false;
if ( isset( $queried_obj->term_id ) ) {
	$on_term = $queried_obj->term_id;
}
 ?>

<div id="main" class="page-blog">

	<?php dbHelper::get_part( 'description', array(
		'title' => $page->post_title,
		'text'  => $page->post_content,
	) ); ?>

	<section id="post-filters">
		<div class="container news-nav">
			<p <!--data-toggle="collapse" data-target="#links" aria-expanded="false"-->Filter Posts</p>
			<?php $categories = get_terms( array( 'taxonomy' => 'category', 'hide_empty' => 0, 'exclude' => 1 ) ); ?>
			<ul <!--class="collapse-sm-down" id="links"-->
				<li <?php if ( !$on_term ) { ?>class="active"<?php } ?>><a href="<?php echo get_permalink( $page_id ); ?>">All Posts</a></li>
				<?php foreach ( $categories as $category ) { ?>
					<li <?php if ( $on_term == $category->term_id ) { ?>class="active"<?php } ?>>
						<a href="<?php echo get_term_link( $category->term_id ); ?>"><?php echo $category->name; ?></a>
					</li>
				<?php } ?>
			</ul>
		</div>
	</section>

	<section id="post-list">
		<div class="container">
			<div class="row">
				<?php while ( have_posts() ) { the_post(); ?>
					<div class="col-md-6">
						<a class="item" href="<?php the_permalink(); ?>">
							<div class="post-head">
								<p class="title"><?php echo dbHelper::shorten_text( get_the_title(), 120 ); ?></p>
								<p class="date">Posted <?php the_time('d.m.Y'); ?></p>
							</div>
							<?php $image = dbHelper::get_featured_url( get_the_ID(), 'blog_thumbnail' );
							if ( !$image ) { $image = dbHelper::get_placeholder(); } ?>
							<div class="image">
								<div class="overlay"></div>
								<div class="background" style="background-image:url('<?php echo $image; ?>');"></div>
							</div>
							<div class="dyn-content"><?php the_excerpt(); ?></div>
							<p class="more">Read More</p>
						</a>
					</div>
				<?php } ?>
			</div>
		</div>
	</section>

	<?php dbHelper::get_part( 'pagination' ); ?>

</div>

<?php get_footer();