<?php
/**
 * Content wrappers
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/global/wrapper-start.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     3.3.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

$page_class = '';

if ( is_search() ) {
	$page_class .= 'page-search';

} elseif ( is_product() ) {
	$page_class = 'single-product';

} else {
	$page_class .= 'page-category category-' . strtolower( str_replace( " ", "_", woocommerce_page_title( false ) ) );

	if ( is_shop() ) {
		$page_class .= ' shop-home';
	}

} ?>
<div id="main" class="<?php echo $page_class; ?>">

	<?php dbHelper::get_part( 'breadcrumbs' );

	dbHelper::get_part( 'woo_message' );

