<?php
/**
 * Single Product Image
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product/product-image.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.5.1
 */

defined( 'ABSPATH' ) || exit;

// Note: `wc_get_gallery_image_html` was added in WC 3.3.2 and did not exist prior. This check protects against theme overrides being used on older versions of WC.
if ( ! function_exists( 'wc_get_gallery_image_html' ) ) {
	return;
}

global $product, $prod_images;

$prod_images = $product->get_gallery_attachment_ids();
if ( $featured_image = get_post_thumbnail_id() ) {
	array_unshift( $prod_images, $featured_image );
}

?><div id="product-images"><?php

	if ( !empty( $prod_images ) ) {

		?><div class="gallery">
			<div class="gallery-prev"></div>

			<div class="gallery-slider"><?php

				$i = 0; foreach ( $prod_images as $image ) {

					$img_src       = wp_get_attachment_image_src( $image, 'full' );
					$image_title   = esc_attr( get_the_title( $image ) );
					$image_caption = esc_attr( get_post_field( 'post_excerpt', $image ) );

					?><div class="gallery-slider-slide" data-index="<?php echo $i; ?>" style="background-image:url('<?php echo $img_src[0]; ?>');">
						<?php /*<img data-src="<?php echo $img_src[0]; ?>" alt="<?php echo $image_title; ?>" />
						<p><?php echo $image_title; ?></p>
						<p><?php echo $image_caption; ?></p>*/ ?>
					</div><?php
				$i++; }

			?></div>

			<div class="gallery-next"></div><?php

			do_action( 'woocommerce_product_thumbnails' );

		?></div><?php

	} else { ?>
		<p>No Images</p>
	<?php } ?>
</div>