jQuery(function($) {

	/* -- Tabs -- */

	$( '.tabs' ).on( 'click', 'li', function() {
		var $tabs = $( this ).closest( '.tabs' );
		var new_tab = $( this ).data( 'tab' );
		$tabs.find( '.active' ).removeClass( 'active' );
		$( 'li[data-tab="' + new_tab + '"]' ).addClass( 'active' );
		$( 'div[data-name="' + new_tab + '"]' ).addClass( 'active' );
	} );

} );