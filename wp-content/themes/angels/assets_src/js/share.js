(function( $ ) {
	'use strict';

	var debug_status = true;

	var debug = function ( comment ) {
		if ( debug_status ) {
			console.log( comment );
		}
	};

	$( 'document' ).ready( function() {

		if ( $( '.share-links' ).length ) {
			$( '.share-links' ).each( function() {

				debug( 'found share links' );

				var share_container = $( this );

				if ( $( this ).find( '[data-social]' ).length ) {

					debug( 'found socials' );

					$( this ).find( '[data-social]' ).each( function( key, val ) {

						$( this ).on( 'click', function( e ) {
							e.preventDefault();

							/* Get window sizes */
							var window_width = $( window ).width();
							var window_height = $( window ).height();

							/* Setup popup dimensions */
							var popup_width = 400;
							var popup_height = 300;
							if ( $( this ).data( 'popup-width' ) ) {
								popup_width = $( this ).data( 'popup-width' );
							}
							if ( $( this ).data( 'popup-height' ) ) {
								popup_height = $( this ).data( 'popup-height' );
							}

							/* Determine social type */
							var social_type = false;
							if ( $( this ).data( 'social' ) ) {
								social_type = $( this ).data( 'social' );
							}

							/* Get popup content */
							var content_title = document.title;
							var content_text = '';
							var content_url = document.location.href;
							if ( $( this ).data( 'social-title' ) ) {
								content_title = $( this ).data( 'social-title' );
							} else if ( share_container.data( 'social-title' ) ) {
								content_title = share_container.data( 'social-title' );
							}
							if ( $( this ).data( 'social-text' ) ) {
								content_text = $( this ).data( 'social-text' );
							} else if ( share_container.data( 'social-text' ) ) {
								content_text = share_container.data( 'social-text' );
							}
							if ( $( this ).data( 'social-url' ) ) {
								content_url = $( this ).data( 'social-url' );
							} else if ( share_container.data( 'social-url' ) ) {
								content_url = share_container.data( 'social-url' );
							}
							debug( 'content_title: ' + content_title );
							debug( 'content_text: ' + content_text );
							debug( 'content_url: ' + content_url );
							debug( 'social_type: ' + social_type );

							/* Build popup URL */
							var popup_url = false;
							switch( social_type ) {
								case 'facebook':
									debug( 'building facebook' );
									popup_url = 'https://www.facebook.com/sharer/sharer.php?u=' + content_url + '&title=' + content_title + '&description=' + content_text;
								break;
								case 'twitter':
									debug( 'building twitter' );
									popup_url = 'http://twitter.com/home?status=' + content_title + ' - ' + content_text + ' - ' + content_url;
								break;
								case 'linkedin':
									debug( 'building linkedin' );
									popup_url = 'https://www.linkedin.com/shareArticle?mini=true&url=' + content_url + '&title=' + content_title + '&summary=' + content_text;
								break;
								/*case 'googleplus':
									debug( 'building googleplus' );
									popup_url = 'https://plus.google.com/share?url=' + content_url + '&title=' + content_title;
								break;*/
							}
							debug( popup_url );

							/* Execute popup URL */
							if ( popup_url ) {
								var popup_top = ( window_height / 2 ) - ( popup_height / 2);
								var popup_left = ( window_width / 2 ) - ( popup_width / 2 );
								window.open( popup_url, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,width=' + popup_width + ',height=' + popup_height + ',top=' + popup_top + ',left=' + popup_left );
							} else {
								debug( 'popup URL failed to build' );
							}

						} );
					} );
				}
			} );
		}

	} );

})( jQuery );