jQuery(function($) {

	//----------| Product Page Quantity |----------//

	$( '.quantity-up' ).on( 'click', function() {
		var input = $( this ).parents( '#quantity' ).find( 'input' );
		var max = parseInt( input.attr( 'max' ) ) || 999;
		var new_quan = parseInt( input.val() ) + 1;

		if ( new_quan <= max ) {
			input.val( new_quan );
		}

		unlock_update_cart();
	} );
	$( '.quantity-down' ).on( 'click', function() {
		var input = $( this ).parents( '#quantity' ).find( 'input' );
		var min = parseInt( input.attr( 'min' ) ) || 0;
		var new_quan = parseInt( input.val() ) - 1;

		if ( new_quan >= min ) {
			input.val( new_quan );
		}

		unlock_update_cart();
	} );

	function unlock_update_cart() {
		var update_button = $( 'input[name="update_cart"]' );
		if ( update_button.length ) {
			if ( update_button.prop( 'disabled' ) ) {
				update_button.prop( 'disabled', false );
			}
		}
	}

} );