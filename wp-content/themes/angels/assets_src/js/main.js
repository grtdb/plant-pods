jQuery(function($) {

	var window_width = 0, window_height = 0, header_height = 0, wpbar_height = 0, window_realestate = 0;

	update_window_sizes();

	$( window ).resize( function() {
		update_window_sizes();
	} );

	function update_window_sizes() {
		window_width = $( window ).width();
		window_height = $( window ).height();
		if ( $( 'header' ).length ) {
			header_height = $( 'header' ).height();
		}
		if ( $( '#wpadminbar' ).length ) {
			wpbar_height = $( '#wpadminbar' ).height();
		}
		window_realestate = window_height - header_height - wpbar_height;
	}

	//----------| DropDown Search |----------//

	$( '#search-button' ).on( 'click', function() {
		if ( $( this ).hasClass( 'active' ) ) {
			$( this ).removeClass( 'active' );
			$( '#drop-search' ).stop().slideUp( 400 );
		} else {
			$( this ).addClass( 'active' );
			$( '#drop-search' ).stop().slideDown( 400 );
			$( '#drop-search input.search' ).focus();
		}
	} );

	//----------| Mobile header Nav |----------//

	$( '#mob-nav' ).on( 'click', function() {
		if ( $( this ).hasClass( 'active' ) ) {
			$( this ).removeClass( 'active' );
			$( '#site-wrapper' ).css( { 'width' : 'auto' } );
			$( 'body' ).removeClass( 'navopen' );
		} else {
			if ( $( '.search-button' ).hasClass( 'active' ) ) {
				$( '.search-button' ).removeClass( 'active' );
				$( '#search-box' ).removeClass( 'active' );
			}
			$( this ).addClass( 'active' );
			$( '#site-wrapper' ).css( { 'width' : window_width } );
			$( 'body' ).addClass( 'navopen' );
		}
	} );

	$( 'body.navopen #site-wrapper' ).on( 'click', function() {
		$( 'body.navopen' ).removeClass( 'navopen' );
		$( '#site-wrapper' ).css( { 'width' : 'auto' } );
		$( '#mob-nav' ).removeClass( 'active' );
	} );

	/* -- Header cart dropdown -- */
	if ( $( '#header-cart-dropdown' ).length ) {
		$( '#header-cart' )
		.on( 'mouseover', function() {
			$( '#header-cart-dropdown' ).stop().slideDown( 200 );
		} )
		.on( 'mouseleave', function() {
			$( '#header-cart-dropdown' ).stop().slideUp( 100 );
		} );
	}

	/* -- Product filter dropdown -- */
	$( '#filter-posts' ).on( 'click', function() {
		if ( $( '#filter-posts-dropdown' ).is( ':visible' ) ) {
			$( '#filter-posts-dropdown' ).stop().slideUp( 100 );
		} else {
			$( '#filter-posts-dropdown' ).stop().slideDown( 200 );
		}
	} );

	/* -- Galleries -- */

	$( '#home-slideshow .gallery' ).dbGallery();

	$( '#store-gallery .gallery' ).dbGallery();

	$( '#hire-slider .gallery' ).dbGallery();

	$( '#product-images .gallery' ).dbGallery();

	$( '.product-carousel .gallery, .category-carousel .gallery' ).dbGallery();

	/* -- History Gallery -- */

	$( '#history-slider .gallery' ).dbGallery( {
		onSlideLoad: function( $gallery, curSlide ) {
			var $cur_slide = $( '#history-slider .gallery .gallery-slider-slide[data-index="' + curSlide + '"]' ).closest( '.owl-item' );
			var prev_year = $cur_slide.prev().find( '.year' ).text() || false;
			var next_year = $cur_slide.next().find( '.year' ).text() || false;
			$gallery.find( '.gallery-prev' ).text( prev_year );
			$gallery.find( '.gallery-next' ).text( next_year );
			$( '#history-slider-nav ul li' ).removeClass( 'active' );
			$( '#history-slider-nav ul li[data-index="' + curSlide + '"' ).addClass( 'active' );
		}
	} );

	$( '#history-slider-nav ul li' ).on( 'click', function() {
		var newSlide = $( this ).data( 'index' );
		$( '#history-slider .gallery .gallery-slider' ).trigger( 'to.owl.carousel', [ newSlide, 250, true] );
	} );

	/* -- Stickies -- */

	/*if ( $( '#main.page-checkout' ).length ) {
		$( '#order_review' ).dbSticky( {
			debug:true,
			onStatusChange: function( $element, status ) {
				if ( status ) {

				} else {

				}
			}
		} );
	}*/

} );