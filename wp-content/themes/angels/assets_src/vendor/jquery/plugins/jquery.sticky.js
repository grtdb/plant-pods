/*!
 Sticky plugin v1.0
 Info: This plugin allows the quick, easy and clean creation of Sticky page elements.
 Author: Matthew Murray ( matthew@exleysmith.com )

 --| Update Log |--

 - v1.0 - Created

 */

( function( $ ) {
    'use strict';

    var defaults = {
        debug: false,
		offset: 50,
		mobile: false,
		tablet: false,
		desktop: true,
        onStatusChange: function( $element, status, elementWidth, elementHeight ) {

        }
    };

    $.fn.dbSticky = function( options ) {

        var settings = $.extend( {}, defaults, options );

        return this.each( function() {
            var $self = $( this );

            if ( settings.debug ) {
                console.log( 'Initialising a STICKY element' );
                console.log( ' - offset: ' + settings.offset );
            }

			doSticky( $self );
			$( window ).resize( function( ev ) {
				doSticky( $self );
			} );

        } );

		function doSticky( $self ) {
			var enableSticky = false;
			if ( isMobile() && settings.mobile ) {
				enableSticky = true;
			} else if ( isTablet() && settings.tablet ) {
				enableSticky = true;
			} else if ( isDesktop() && settings.desktop ) {
				enableSticky = true;
			}

			if ( enableSticky ) {
				$( window ).scroll( function( ev ) {
					var windowOffset = $( document ).scrollTop();
					var elementOffset = $self.offset().top;
					var stickySwitch = elementOffset - settings.offset;
					var stuck = false;

					if ( windowOffset > stickySwitch ) {
						if ( $self.hasClass( 'sticky_unstuck' ) ) {
							$self.removeClass( 'sticky_unstuck' );
						}
						$self.addClass( 'sticky_stuck' );
						stuck = true;
					} else {
						if ( $self.hasClass( 'sticky_stuck' ) ) {
							$self.removeClass( 'sticky_stuck' );
						}
						$self.addClass( 'sticky_unstuck' );
						stuck = false;
					}

					settings.onStatusChange( $self, stuck, $self.outerWidth(), $self.outerHeight() );

					if ( settings.debug ) {
						console.log( 'elementOffset: ' + elementOffset + ' windowOffset: ' + windowOffset + ' stickySwitch: ' + stickySwitch );
					}
				} );
			}
		}

    };
} )( jQuery );
