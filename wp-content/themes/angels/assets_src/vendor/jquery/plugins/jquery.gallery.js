/*!
 Gallery plugin v1.4.2
 Info: This plugin allows the quick, easy and clean creation of sliders, galleries and carousels with the Owl Carousel plugin.
 Author: Matthew Murray ( matthew@exleysmith.com )

 --| Data Attributes Documentation |--

 Dependancies
 - Owl Carousel 2 plugin
 - MatchMedia responsive jQuery library

 Data tags On Gallery
 - "pagination" ( Turns on or off the owl carousel pagination )
 - "auto_play" ( Turns on or off auto slide rotation )
 - "zoom" ( Turns on or off slider zoom )
 - "force_nav" ( Forces the next / previous buttons to always show regardless of slide count )
 - "auto_height", "auto_height_tab", "auto_height_mob" ( Turns on or off auto height at various break points )
 - "lazy_load" ( Turn on or off owl carousels lazy load option )
 - "loop" ( Turn on infinate loop, default is off )
 - "fadecontent" ( Element containing content to fade in )

 Data tags On Slider
 -

 Data tags On Carousel
 - "items", "items_tab", "items_mob" ( specify number of items to show )
 - "spacing" ( specify pixel spacing between slides )
 - "center" ( center the selected slide )

 Using Slider & Carousel togther
 - Both within same "gallery" div wrapper.
 - Must add data-index to both slider slides and carousel slides to link their position in markup.
 - Carousel will trigger Slider and vice versa.

 --| Update Log |--

 - v1.2.0 - Added loader overlay.
 - v1.3.0 - Added .zoom.zoomable class to zoomable slides for css icon. Fixed manual advance effecting other sliders.
 - v1.3.1 - Zoom initialised by gallery data option. Renamed slideshow to slider. Always show nav arros option. Added data options documentation.
 - v1.3.2 - dataNumTab and dataNumMob now default to dataNum if not set rather than NULL. Updated Documentation with dependancies.
 - v1.4.0 - slideLoad callback to remove next/prev if no next/prev available and added selected class to carousel for the current slide
 - v1.4.1 - added onInit callback functions
 - v1.4.2 - added fadecontent tag for easy fading in of slide content
 */

( function ( $ ) {
    'use strict';

    var defaults = {
        showLoader: true,
        debug: true,
		onSliderInit: function( $gallery ) {},
		onCarouselInit: function( $gallery ) {},
        onSlideLoad: function ( $gallery, curSlide ) {}
    };

    $.fn.dbGallery = function ( options ) {

        var settings = $.extend( {}, defaults, options );
        var curSlide = 0;
		var sliderInit = function ( $gallery, dataFadeContent ) {
			if ( dataFadeContent && !isMobile() ) {
				$gallery.find( '.gallery-slider-slide[data-index="0"] ' + dataFadeContent ).fadeIn();
			}
			settings.onSliderInit( $gallery );
		}
		var carouselInit = function( $gallery ) {
			settings.onCarouselInit( $gallery );
		}
        var slideLoad = function ( $gallery, curSlide, numSlides, dataNum, dataLoop, combined, dataFadeContent ) {
			if ( dataFadeContent && !isMobile() ) {
				$gallery.find( dataFadeContent ).hide();
				$gallery.find( '.gallery-slider-slide[data-index="' + curSlide + '"] ' + dataFadeContent ).fadeIn();
			}
            if ( !dataLoop ) {
                if ( curSlide < 1 ) {
                    $gallery.find( '.gallery-prev' ).hide();
                } else {
                    $gallery.find( '.gallery-prev' ).show();
                }

                var items = ( dataNum || 1 );
                if ( combined ) {
                    items = 1;
                }

                if ( curSlide >= numSlides - items ) {
                    $gallery.find( '.gallery-next' ).hide();
                } else {
                    $gallery.find( '.gallery-next' ).show();
                }

                $gallery.find( '.gallery-carousel .gallery-carousel-slide' ).removeClass( 'selected' );
                $gallery.find( '.gallery-carousel .gallery-carousel-slide[data-index=' + curSlide + ']' ).addClass( 'selected' );
                settings.onSlideLoad( $gallery, curSlide );
            }
        }

        return this.each( function () {
            var $self = $( this );

            if ( settings.showLoader ) {
                $self.prepend( '<div class="gallery-load"></div>' );
                $self.find( '.gallery-load' ).show();
                $self.find( '.gallery-slider' ).css( { opacity: 0 } );

                $( window ).load( function () {
                    $self.find( '.gallery-slider, .gallery-carousel' ).animate( { opacity: 1 }, 600, function () {
                        $self.find( '.gallery-load' ).fadeOut();
                    });
                });
            }

            var dataStage = $self.data( 'stage' ) || false; // CHECK FOR STAGE PADDING

            var numSlides = $self.find( '.gallery-slider-slide' ).length; // COUNT THE SLIDES
            var numSlides2 = $self.find( '.gallery-carousel-slide' ).length;
            var combined = ( numSlides > 0 && numSlides2 );
            if ( numSlides < 1 && numSlides2 > 0 ) {
                numSlides = numSlides2;
            } // carousel only, set num slides to carousel num

            var dataNum = 1;
            var dataPagination = $self.data( 'pagination' ) || false; // CHECK FOR PAGIANTION

            var dataAutoPlay = $self.data( 'auto_play' ) || false; // CHECK AUTO PLAY
            var dataAutoOn = false, dataAutoSpeed = 3000;
            if ( dataAutoPlay ) {
                dataAutoOn = true;
            }

			var dataFadeContent = $self.data( 'fadecontent' ) || false; // CHECK CONTENT FADE

            var dataZoom = $self.data( 'zoom' ) || false; // CHECK FOR ZOOM
            var dataLazyLoad = $self.data( 'lazy_load' ) || false; // CHECK FOR LAZY LOAD
            var dataForceNav = $self.data( 'forcenav' ) || false; // CHECK FOR FORCED NAV

            var dataAutoHeight = false;
            var dataAutoMob = $self.data( 'auto_height_mob' ); // CHECK FOR AUTO HEIGHT
            var dataAutoTab = $self.data( 'auto_height_tab' );
            var dataAutoDesk = $self.data( 'auto_height' );

            if ( isMobile() && dataAutoMob ) {
                dataAutoHeight = true;
            } else if ( isTablet() && dataAutoTab ) {
                dataAutoHeight = true;
            } else if ( dataAutoDesk ) {
                dataAutoHeight = true;
            }

            var dataLoop = $self.data( 'loop' ) || false;

            if ( settings.debug ) {
                console.log( 'Initialising a GALLERY' );
                console.log( ' - slider slides: ' + numSlides + '; carousel slides: ' + numSlides2 +
                    '; dataPagination: ' + dataPagination + '; dataAutoPlay: ' + dataAutoPlay +
                    '; dataZoom: ' + dataZoom + '; dataForceNav: ' + dataForceNav +
                    '; dataAutoHeight: ' + dataAutoHeight + '; dataAutoMob: ' + dataAutoMob +
                    '; dataAutoTab: ' + dataAutoTab + '; dataAutoDesk: ' + dataAutoDesk +
                    '; dataLazyLoad: ' + dataLazyLoad + '; dataStage: ' + dataStage +
                    '; dataLoop: ' + dataLoop + '; dataFadeContent: ' + dataFadeContent
                );
            }

            // MAKE A slider IF EXISTS
            if ( $self.find( '.gallery-slider' ).length ) {

                if ( typeof $self.find( '.gallery-slider' ).data( 'owlCarousel' ) !== 'undefined' ) {
                    $self.find( '.gallery-slider' ).trigger( 'destroy.owl.carousel' );
                    if ( defaults.debug ) { console.log( 're-initialising' ); }
                } else {
                    if ( defaults.debug ) { console.log( 'first init' ); }
                }

                if ( numSlides <= 1 && !dataForceNav ) {
                    $self.find( '.gallery-prev, .gallery-next' ).hide(); // hide nav if 1 slide
                }

                if ( numSlides >= 2 ) {

                    var sliderOptions = {
                        autoplay: dataAutoOn,
                        autoplayTimeout: dataAutoPlay,
                        autoplaySpeed: dataAutoSpeed,
                        items: 1,
                        loop: dataLoop,
                        autoHeight: dataAutoHeight,
                        pagination: dataPagination,
                        lazyLoad: dataLazyLoad,
                        stagePadding: dataStage,
                        onInitialized: function () {
                            if ( dataPagination ) {
                                if ( dataPagination ) {
                                    this.$element.find( '.owl-controls, .owl-controls .owl-dots' ).show();
                                } else {
                                    this.$element.find( '.owl-controls .owl-dots' ).hide();
                                }
                            } else {
                                this.$element.find( '.owl-controls' ).hide();
                            }
							sliderInit( $self, dataFadeContent );
                        }
                    };

                    $self.find( '.gallery-slider' ).owlCarousel( sliderOptions );
                }
            }

            // MAKE A CAROUSEL IF EXISTS
            if ( $self.find( '.gallery-carousel' ).length ) { // && ( numSlides > 1 )

                dataNum = $self.find( '.gallery-carousel' ).data( 'items' ); // CHECK FOR FORCED DISPLAY NUMBER
                var dataNumTab = $self.find( '.gallery-carousel' ).data( 'items_tab' ) || dataNum;
                var dataNumMob = $self.find( '.gallery-carousel' ).data( 'items_mob' ) || dataNum;
                if ( isMobile() ) {
                    dataNum = dataNumMob;
                } else if ( isTablet() ) {
                    dataNum = dataNumTab;
                } // set dataNum to responsive num

                var dataCenter = $self.data( 'center' ) || false;

                if ( settings.debug ) {
                    console.log( ' - Making a CAROUSEL' );
                    console.log( ' - dataNum: ' + dataNum + '; dataNumTab: ' + dataNumTab + '; dataNumMob: ' + dataNumMob + '; dataNum: ' + dataNum + '; dataCenter: ' + dataCenter );
                }

                var respond;
                if ( isMobile() ) {
                    if ( numSlides <= ( dataNum || 1 ) && !dataForceNav ) {
                        $self.find( '.gallery-prev, .gallery-next' ).hide(); // hide nav if 1 slide
                    }
                    if ( dataNum === 1 ) {
                        respond = { 1: { items: 1 } };
                    } else {
                        respond = {
                            1: { items: 1 },
                            400: { items: 2 },
                            600: { items: 3 },
                            800: { items: 4 },
                            1000: { items: 5 }
                        };
                    }
                } else {
                    if ( numSlides <= ( dataNum || 4 ) && !dataForceNav ) {
                        $self.find( '.gallery-prev, .gallery-next' ).hide(); // hide nav if 1 slide
                    }
                    if ( isTablet() ) {
                        respond = { 1: { items: dataNum || 3 } };
                    } else {
                        respond = { 1: { items: dataNum || 4 } };
                    }
                }

                var spacing = 0;
                if ( $self.find( '.gallery-carousel' ).data( 'spacing' ) ) {
                    spacing = $self.find( '.gallery-carousel' ).data( 'spacing' );
                }

				var carouselOptions = {
                    autoPlay: dataAutoPlay,
                    responsive: respond,
                    autoHeight: dataAutoHeight,
                    autoplaySpeed: dataAutoSpeed,
                    center: dataCenter,
                    loop: dataLoop,
                    margin: spacing,
                    lazyLoad: dataLazyLoad,
                    stagePadding: dataStage,
                    onInitialized: function () {
                        if ( dataPagination ) {
                            this.$element.find( '.owl-controls' ).show();
                        } else {
                            this.$element.find( '.owl-controls' ).hide();
                        }

						carouselInit( $self );
                        slideLoad( $self, curSlide, numSlides, dataNum, dataLoop, combined, dataFadeContent );
                    }
				};

                $self.find( '.gallery-carousel' ).owlCarousel( carouselOptions );
            }

            // GALLERY CONTROLS
            $self.find( '.gallery-prev' ).on( 'click', function () { // previous slide click
                $self.find( '.gallery-slider, .gallery-carousel' ).trigger( 'prev.owl.carousel' );
                curSlide -= 1;

                if ( settings.debug ) { console.log( ' # Gallery Prev Clicked; newSlide: ' + curSlide ); }
            } );
            $self.find( '.gallery-next' ).on( 'click', function () { // next slide click
                $self.find( '.gallery-slider, .gallery-carousel' ).trigger( 'next.owl.carousel' );
                curSlide += 1;

                if ( settings.debug ) { console.log( ' # Gallery Next Clicked; newSlide: ' + curSlide ); }
            } );

			if ( combined ) { // Combined Slider & Carousel
				$self.find( '.gallery-carousel' ).on( 'click', '.gallery-carousel-slide', function () { // carousel slide click
					var newSlide = $( this ).data( 'index' );
					$self.find( '.gallery-slider' ).trigger( 'to.owl.carousel', [ newSlide, 250, true ] );
					curSlide = newSlide;
					slideLoad( $self, curSlide, numSlides, dataNum, dataLoop, combined, dataFadeContent );

					if ( settings.debug ) { console.log( ' # Gallery Carousel item Clicked; newSlide: ' + newSlide ); }
				} );
				$self.find( '.gallery-slider' ).on( 'translated.owl.carousel', function() { // slider manual advance
					var newSlide = $( '.gallery-slider .active .gallery-slider-slide' ).data( 'index' );
					$self.find( '.gallery-carousel' ).trigger( 'to.owl.carousel', [ newSlide, 250, true ] );
					curSlide = newSlide;
					slideLoad( $self, curSlide, numSlides, dataNum, dataLoop, combined, dataFadeContent );

					if ( settings.debug ) { console.log( ' # Gallery Slider Dragged; newSlide: ' + newSlide ); }
				} );
            } else { // Single Slider or Carousel
				$self.find( '.gallery-slider' ).on( 'translated.owl.carousel', function() {
                    var newSlide = $( '.gallery-slider .active:first .gallery-slider-slide' ).data( 'index' );
                    curSlide = newSlide;
                    slideLoad( $self, curSlide, numSlides, dataNum, dataLoop, combined, dataFadeContent );
                } );
                $self.find( '.gallery-carousel' ).on( 'translated.owl.carousel', function() {
                    var newSlide = $( '.gallery-carousel .active:first .gallery-carousel-slide' ).data( 'index' );
                    curSlide = newSlide;
                    slideLoad( $self, curSlide, numSlides, dataNum, dataLoop, combined, dataFadeContent );
                } );
            }

            // GALLERY ZOOM
            if ( dataZoom === true ) {
                $( '.gallery-slider .gallery-slider-slide' ).each( function() { // loop through slides
                    var curSlide = $( this );
                    var tipWrap = $( curSlide ).find( '.tip-wrap' ); // tip wrap selector
                    var toZoom = $( curSlide ).find( '.zoom' );	// zoom selector
                    var zoomImg = $( toZoom ).find( 'img' ); // zoom IMG selector

                    $( window ).load( function() {
                        if ( zoomImg.length ) {
                            var zoomW = zoomImg.width();
                            var zoomH = zoomImg.height();
                            var zoomNatW = zoomImg.get( 0 ).naturalWidth;
                            var zoomNatH = zoomImg.get( 0 ).naturalHeight;
                            //var zooming;

                            if ( zoomNatW > zoomW || zoomNatH > zoomH ) { // Only enable zoom if the image natural dimensions are greater than the <img> dimensions
                                $( toZoom ).addClass( 'zoomable' ).zoom();
                            } else {
                                $( tipWrap ).addClass( 'nozoom' ).hide();
                            }
                        }
                    });
                } ).on( 'tap', '.tip-wrap', function() {
                    $(this).hide();
                } ).on( 'tap', '.zoom', function() {
                    if ( !$( this ).prev( '.tip-wrap' ).hasClass( 'nozoom' ) ) {
                        $( this ).prev( '.tip-wrap' ).show();
                    }
                } );
                $( this ).find( '.zoom, .tip-wrap:not( .nozoom )' ).hammer(); // enable hammer

                $( '.gallery-slider' ).on( 'translated.owl.carousel', function() { // show the tip wrap again on slide change
                    $.each( $( '.gallery-slider .tip-wrap' ), function() {
                        if ( !$( this ).hasClass( 'nozoom' ) ) {
                            $( this ).show();
                        }
                    } );
                } );
            }
        } );
    };
} )( jQuery );
