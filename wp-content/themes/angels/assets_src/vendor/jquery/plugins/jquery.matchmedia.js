/*! matchMedia() polyfill - Test a CSS media type/query in JS. Authors & copyright (coffee) 2012: Scott Jehl, Paul Irish, Nicholas Zakas, David Knight. Dual MIT/BSD license */
window.matchMedia || (window.matchMedia = function () {
    var b = (window.styleMedia || window.media);
    if (!b) {
        var c = document.createElement("style"), a = document.getElementsByTagName("script")[0], d = null;
        c.type = "text/css";
        c.id = "matchmediajs-test";
        a.parentNode.insertBefore(c, a);
        d = ("getComputedStyle" in window) && window.getComputedStyle(c, null) || c.currentStyle;
        b = {
            matchMedium: function (e) {
                var f = "@media " + e + "{ #matchmediajs-test { width: 1px; } }";
                if (c.styleSheet) {
                    c.styleSheet.cssText = f
                } else {
                    c.textContent = f
                }
                return d.width === "1px"
            }
        }
    }
    return function (e) {
        return {matches: b.matchMedium(e || "all"), media: e || "all"}
    }
}());

var media_queries = {
    laptop: window.matchMedia('(min-width:992px) and (max-width: 1099px)'),
    tablet: window.matchMedia('(min-width:768px) and (max-width: 991px)'),
    mobile: window.matchMedia('(max-width:767px)')
}

function refreshMediaQueries() {
    media_queries.laptop = window.matchMedia('(min-width:992px) and (max-width: 1099px)');
    media_queries.tablet = window.matchMedia('(min-width:768px) and (max-width: 991px)');
    media_queries.mobile = window.matchMedia('(max-width:767px)');
}

function isMobile() {
    return media_queries.mobile.matches;
}

function isTablet() {
    return media_queries.tablet.matches;
}

function isLaptop() {
    return media_queries.laptop.matches;
}

function isDesktop() {
    return (!media_queries.tablet.matches && !media_queries.mobile.matches);
}

jQuery(function ($) {
    $(window).on('resize', refreshMediaQueries);
});
