			<footer>
				<section id="footer" class="clearfix">
					<div class="container">
						<div class="row">
							<div class="col-md-6 col-lg-3">
								<?php wp_nav_menu( array( 'theme_location' => 'menu', 'container' => false ) ); ?>
							</div>
							<div class="col-md-6 col-lg-3 d-none d-sm-block">
								<?php wp_nav_menu( array( 'theme_location' => 'cat_menu', 'container' => false ) ); ?>
							</div>
							<?php $stores = get_posts( array( 'post_type' => 'stores', 'posts_per_page' => 2, 'orderby' => 'menu_order', 'order' => 'ASC' ) );
							if ( $stores ) {
								foreach ( $stores as $store ) { ?>
									<div class="store col-md-3 d-none d-md-block">
										<p class="footer-title"><?= $store->post_title; ?></p>
										<?php if ( $address = get_field( 'store_address', $store->ID ) ) { ?>
											<address><?= $address; ?></address>
										<?php }
										if ( $telephone = get_field( 'telephone_number', $store->ID ) ) { ?>
											<a href="tel:<?= $telephone; ?>">T: <?= $telephone; ?></a>
										<?php }
										if ( $email = get_field( 'email_address', $store->ID ) ) { ?>
											<a href="mailto:<?= $email; ?>">E: <?= $email; ?></a>
										<?php } ?>
									</div>
								<?php }
							} ?>
						</div>
					</div>
				</section>
				<section id="footer-bottom">
					<div class="container">
						<div class="row">
							<div class="col-md-4 text-md-left">
								<p id="footer-copyright">
									<?= get_option( 'copyright_text' ); ?>
								</p>
							</div>
							<div class="col-md-4">
								<div id="footer-social">
									<p>Follow Us</p>
									<?php
									foreach ( dbHelper::get_socials() as $social_key => $social_name ) {
										$social_title = get_option( $social_key . '_title' );
										$social_url = get_option( $social_key . '_url' );
										if ( $social_url ) {
											?>
											<a class="social-<?= $social_key; ?>" href="<?= $social_url; ?>" title="<?= $social_title; ?>" target="_blank">
												<span class="fa fa-<?= $social_key; ?>"></span>
											</a>
											<?php
										}
									}
									?>
								</div>
							</div>
							<div class="col-md-4 text-md-right d-none d-sm-block">
								<span id="footer-logo" title="Designed and Built by Exley & Smith ltd">
									<a href="http://www.angels.uk.com" target="_blank"><img src="<?= get_template_directory_uri(); ?>/assets/img/footer-logo.png" alt="Angles Fancy Dress" /></a>
								</span>
							</div>
						</div>
					</div>
				</section>
			</footer>
		</div>
		<?php wp_footer();?>

		<script>
			window.ajaxurl || (window.ajaxurl = '<?= admin_url( 'admin-ajax.php' ); ?>');
		</script>
	</body>
</html>
