<?php // Functions File

// Set directory vars
$theme_url = get_stylesheet_directory_uri();
$theme_dir = get_stylesheet_directory();

// Auto activate plugin
function run_activate_plugin( $plugin ) {
	$current = get_option( 'active_plugins' );
	include_once( ABSPATH . 'wp-admin/includes/plugin.php' );
	$plugin = plugin_basename( trim( $plugin ) );

	if ( !in_array( $plugin, $current ) ) activate_plugin( $plugin );

	return null;
}

add_theme_support( 'post-thumbnails' );



// Activate required plugins
if ( !class_exists('dbHelper') ) run_activate_plugin( 'db-wp-core/db-wp-core.php' );

// Styles & Scripts
if ( !is_admin() ) {
	if ( dbHelper::is_env( 'dev' ) ) {
		dbHelper::add_styles( [
			'https://fonts.googleapis.com/css?family=Roboto',
			$theme_url . '/assets/css/src/vendor.css',
			$theme_url . '/assets/css/src/core.css',
		] );
		dbHelper::add_scripts( [
			$theme_url . '/assets/js/dist/jquery.min.js',
			$theme_url . '/assets_src/js/quantity.js',
			$theme_url . '/assets_src/js/share.js',
			$theme_url . '/assets_src/js/tabs.js',
			$theme_url . '/assets_src/js/main.js',
		] );
	} else {
		dbHelper::add_styles( [
			'https://fonts.googleapis.com/css?family=Roboto',
			$theme_url . '/assets/css/dist/vendor.min.css',
			$theme_url . '/assets/css/dist/core.min.css',
		] );
		dbHelper::add_scripts( [
			$theme_url . '/assets/js/dist/lib.min.js',
			$theme_url . '/assets/js/dist/core.min.js',
		] );
	}
}

// Includes
foreach ( glob( $theme_dir . '/includes/*.php' ) as $file ) {
	include_once( $file );
}

// Declare woocommerce support
add_action( 'after_setup_theme', function() {
    add_theme_support( 'woocommerce' );
} );

// Change Exerpt more icon
add_filter( 'excerpt_more', function( $more ) {
    return '...';
} );