<?php

$contact_settings = new dbOptionsPage( 'Contact Settings', 'contact_options' );

$contact_settings->add_section( 'contact_details', array(
	'label' => 'Contact Information',
	'fields' => array(
		'contact_email' => array( 'Contact Email Address', 'input', 'width:40%;' ),
		'contact_phone' => array( 'Contact Phone Number', 'input', 'width:40%;' ),
	)
) );


$contact_settings->add_section( 'contact_form_details', array(
	'label' => 'Contact Form Details',
	'fields' => array(
		'contact_form_send_to' => array( 'Additional Contact Form Addresses (comma seperated)', 'input', 'width:100%;' ),
		'contact_form_success' => array( 'Success Message (messaged displayed when successfully submitted form)', 'input', 'width:100%;' ),
		'contact_form_enquiry_options' => array( 'Options for the Enquiry type drop down (Commar seperated)', 'input', 'width:100%;' ),
	)
) );