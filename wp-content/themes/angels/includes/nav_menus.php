<?php /* Custom menus */

register_nav_menus( array(
	'header_nav_left' => 'Header Navigation (Left)',
	'header_nav_right' => 'Header Navigation (Right)',
	'menu' => 'Information menu',
	'cat_menu' => 'Categories menu',
) );