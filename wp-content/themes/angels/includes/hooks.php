<?php // Include layout functions
foreach ( glob( get_template_directory() . '/includes/hook-snippets/*.php' ) as $file ) {
	include_once( $file );
}

// Header
add_action( 'db_location_header_mob', 'db_mobile_buttons' );
add_action( 'db_location_header_cart', 'db_header_cart' );

// Move breadcrumbs
remove_action( 'woocommerce_before_main_content', 'woocommerce_breadcrumb', 20, 0 );
add_action( 'db_location_breadcrumbs', 'woocommerce_breadcrumb', 1, 0 );

// Remove unnecessary bits from product archives
add_filter( 'woocommerce_show_page_title', function(){} );
remove_action( 'woocommerce_before_shop_loop', 'woocommerce_catalog_ordering', 30 );
remove_action( 'woocommerce_before_shop_loop', 'woocommerce_result_count', 20 );
remove_action( 'woocommerce_before_shop_loop', 'woocommerce_pagination', 30 );
remove_action( 'woocommerce_after_shop_loop', 'woocommerce_catalog_ordering', 10 );
remove_action( 'woocommerce_after_shop_loop', 'woocommerce_result_count', 20 );

// Add archive wrapper for title / desc
add_action( 'woocommerce_archive_description', 'db_description_wrapper', 9 );
add_action( 'woocommerce_archive_description', 'db_description_wrapper_close', 11 );

// Add archive filters wrappers
add_action( 'woocommerce_archive_description', 'db_sorting_wrapper', 20 );
add_action( 'db_sorting_wrapper_inside', 'woocommerce_result_count', 10 );
add_action( 'woocommerce_archive_description', 'woocommerce_catalog_ordering', 30 );
add_action( 'woocommerce_archive_description', 'db_sorting_wrapper_close', 50 );
add_action( 'woocommerce_archive_description', 'db_sorting_dropdown', 60 );

// Remove woocommerce sidebar
remove_action( 'woocommerce_sidebar', 'woocommerce_get_sidebar', 10 );

// Product list
add_action( 'woocommerce_before_shop_loop_item', 'db_product_list_item_top', 20 );
add_action( 'woocommerce_before_shop_loop_item_title', 'db_product_list_item_middle', 20 );
add_action( 'woocommerce_after_shop_loop_item', 'db_product_list_item_bottom', 1 );
remove_action( 'woocommerce_after_shop_loop_item', 'woocommerce_template_loop_add_to_cart', 10 );

// Categories list
add_action( 'woocommerce_before_subcategory', 'db_category_list_item_top', 20 );
remove_action( 'woocommerce_before_subcategory_title', 'woocommerce_subcategory_thumbnail', 10 );
add_action( 'woocommerce_before_subcategory_title', 'db_category_list_item_middle', 20 );
add_action( 'woocommerce_after_subcategory', 'db_category_list_item_bottom', 1 );

// Product page stuff
remove_action ( 'woocommerce_before_single_product', 'wc_print_notices', 10 );
add_action ( 'db_woo_messages', 'wc_print_notices', 1 );

add_action( 'woocommerce_before_single_product_summary', 'db_product_page_start', 1 );
add_action( 'woocommerce_before_single_product_summary', 'db_product_wrap_start', 30 );

	add_action( 'woocommerce_single_product_summary', 'db_product_summary_start',               1 );
	add_action( 'woocommerce_single_product_summary', 'db_product_code',                        6 );
	add_action( 'woocommerce_single_product_summary', 'db_product_price_wrap_start',            9 );
	remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_rating',  10 );
	add_action( 'woocommerce_single_product_summary', 'db_product_price_wrap_end',              11 );
	remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_excerpt', 20 );
	add_action( 'woocommerce_single_product_summary', 'db_product_summary_mid',                 25 );
	remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_meta',    40 );
	remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_sharing', 50 );
	add_action( 'woocommerce_single_product_summary', 'db_product_summary_end',                 60 );

add_action( 'woocommerce_after_single_product_summary', 'db_product_tabs',                         1 );
add_action( 'woocommerce_after_single_product_summary', 'db_product_wrap_end',                     5 );
remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_output_product_data_tabs', 10 );
//add_action( 'woocommerce_after_single_product_summary', 'db_product_reviews',                    10 );
remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_upsell_display',           15 );
remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_output_related_products',  20 );
add_action( 'woocommerce_after_single_product_summary', 'db_product_relateds',                     40 );
add_action( 'woocommerce_after_single_product_summary', 'db_product_recents',                      45 );

// Product tracking
remove_action( 'template_redirect', 'wc_track_product_view', 20 );
add_action( 'template_redirect', 'db_track_recently_viewed_products', 20 );

// Account wrappers
add_action( 'woocommerce_account_navigation', 'db_account_nav_start', 1 );
add_action( 'woocommerce_account_navigation', 'db_account_nav_end', 20 );
add_action( 'woocommerce_account_navigation', 'db_account_content_start', 21 );
add_action( 'woocommerce_account_content', 'db_account_content_end', 40 );