<?php /* Custom Image Sizes
Dimension name, X size, Y size, Crop (True) || Whitespace (False) */

add_image_size( 'admin_preview', 60, 60, true ); // admin preview

add_image_size( 'history_slide', 300, 300, true ); // History slider inner

add_image_size( 'product_carousel', 185, 250, false ); // Product carousel featured

add_image_size( 'category_carousel', 255, 270, false ); // Category carousel / category thumbnail (in post list)

add_image_size( 'blog_thumbnail', 540, 270, true ); // Blog thumbnail
add_image_size( 'blog_detail', 780, 390, true ); // Blog detail

add_image_size( 'store_thumbnail', 540, 380, true ); // Store thumbnail

add_image_size( 'mobile_category', 60, 65, true ); // Homepage category thumbnails

add_image_size( 'default_gallery_slider', 1110, 555, true ); // Default gallery slider
add_image_size( 'default_gallery_carousel', 170, 85, true ); // Default gallery carousel

add_image_size( 'homepage_slider', 1500, 450, true ); // Homepage slider

add_image_size( 'hire_gallery_slider', 710, 710, false ); // Hire gallery slider
add_image_size( 'hire_gallery_carousel', 100, 100, false ); // Hire gallery carousel