<?php
global $db_process_form;
$db_process_form = new dbProcessForm();

class dbProcessForm {
	public $errors = array();
	public $cur_time;
	public $success = 'Thank you for your message. We will get back to you as soon as possible';
	public $address_dev = 'matthew@exleysmith.com';
	public $address_admin = ''; // Client Email Adress
	public $address_from = 'no-reply'; // Client Reply too Adress
	public $address_from_name;
	public $general_error = 'There was a problem sending your message. Please try again later';
	protected $send_via_smtp = false;
	protected $mailchimp_api = false;
	protected $post;
	protected $mail;

	public function __construct() {

		$this->address_from_name = get_bloginfo( 'name' );
		$this->post = get_post();
		$this->cur_time = time();

		$forms = array(
			'contact_form_submission',
		);

		foreach( $forms as $form ) {
			add_action( 'wp_ajax_nopriv_' . $form, array( $this, 'process_forms_ajax' ) );
			add_action( 'wp_ajax_' . $form, array( $this, 'process_forms_ajax' ) );
		}

		if ( !dbHelper::is_env( 'live' ) ) {
			$this->address_admin = 'matthew@exleysmith.com';
		}

		// Hook on to the 'wp' action to process the forms so the $post variable is available
		//add_action( 'wp', array( $this, 'process_forms' ) );
	}

	/* Deal with AJAX requests */
	public function process_forms_ajax() {

		switch( $_POST['action'] ) {
			case 'contact_form_submission':
				$this->process_contact_form();
			break;
		}

		$json = array();

		if ( empty( $this->errors ) ) {
			$json['success'] = $this->success;
		} else {
			$json['errors'] = $this->errors;
		}

		wp_send_json( $json );
	}

	/* Non AJAX requests */
	public function process_forms() {

		switch( $_POST['action'] ) {
			case 'contact_form_submission':
				$this->process_contact_form( false );
			break;
		}
	}

	/* Send via SMTP */
	private function setup_SMTP() {
		$this->mail->IsSMTP(); 						  // telling the class to use SMTP
		$this->mail->Host       = "smtp.mailgun.org"; // SMTP server
		$this->mail->SMTPDebug  = false;              // enables SMTP debug information; 1 = errors and messages, 2 = messages only
		$this->mail->SMTPAuth   = true;               // enable SMTP authentication
		$this->mail->Port       = 587;                // set the SMTP port for the GMAIL server
		$this->mail->Username   = "";                 // SMTP account username
		$this->mail->Password   = "";                 // SMTP account password
	}

	/* Process Contact Form */
	private function process_contact_form( $is_ajax = true ) {

		if ( $this->validate_contact_form() ) {

			// setup mailer
			require_once( get_stylesheet_directory() . '/includes/classes/class.phpmailer.php' );
			$this->mail = new PHPMailer();
			if ( $this->send_via_smtp ) {
				$this->setup_SMTP();
			}

			// get submitted details
			$name = strip_tags( $_POST['contact_name'] );
			$email = strip_tags( $_POST['contact_email'] );
			$message = htmlspecialchars( $_POST['contact_message'] );

			// set subject
			$this->mail->Subject = 'New Website Enquiry';

			//add reply to & from
			$this->mail->addReplyTo( $email );
			$this->mail->SetFrom( $this->address_from, $this->address_from_name );

			// add addresses
			$this->mail->addAddress( $this->address_admin );
			$other_addresses = get_option( 'contact_form_send_to' );
			if ( $other_addresses ) {
				$other_addresses = explode( ",", $other_addresses );
				foreach ( $other_addresses as $other_address ) {
					$mail->addAddress( $other_address );
				}
			}
			$this->mail->addBcc( $this->address_dev );

			// set body
			ob_start();

			?><p>You have received a message from the Contact Form on the <?php echo $this->address_from_name; ?> website:</p>
			<p>Name: <?php echo $name; ?></p>
			<p>Email Address: <?php echo $email; ?></p>

			<br /><p>Message:<br /><?php echo $message; ?></p><?php

			$this->mail->Body = ob_get_clean();

			$this->mail->isHTML( true );

			// subscribe
			if ( isset( $_POST[ 'list_id' ] ) ) {
				$this->subscribe_tolist( $_POST[ 'list_id' ], $email );
			}

			// send mail
			if ( $this->mail->send() || dbHelper::is_env( 'dev' ) ) {
				$this->success = get_option( 'contact_form_success' );

				if ( !$is_ajax ) {
					wp_redirect( add_query_arg( 'success', '1', get_permalink( $this->post->ID ) ), 302 );
				}
			} else {
				$this->errors['mail'] = $this->general_error;
			}
		}
	}

	/*private function subscribe_tolist( $list_id, $email ) {

		dbHelper::log( 'Subscribing ' . $email . ' to list ' . $list_id, 'email-signup.txt' );

		if ( $this->mailchimp_api ) {
			include_once( get_template_directory() . '/includes/Mailchimp.php' );

			$MailChimp = new Mailchimp( $this->mailchimp_api );

			try {
				$result = $MailChimp->lists->subscribe(
					$list_id, array( 'email' => $email ),
					array(), false, true, false, false
				);
				dbHelper::log( '  ...Successfully added to list!', 'email-signup.txt' );
				return true;
			} catch( Mailchimp_Error $e ){
				$this->errors[ 'mail' ] = 'Failed to subscribe you to our mailing list: ' . $e->getMessage();
				dbHelper::log( '  ...Failed to add to list.', 'email-signup.txt' );
				return false;
			}
		} else {
			dbHelper::log( '  ...No mailchip API key set!', 'email-signup.txt' );
		}
	}*/

	/* Validate */
	private function validate_contact_form() {

		if ( !empty( $_POST[ 'contact_url' ] ) ) {
			$this->errors[ 'contact_url' ] = 'Anti-spam field should be left blank';
			return false;
		}

		if ( empty( $_POST[ 'contact_name' ] ) ) {
			$this->errors[ 'contact_name' ] = 'Please tell us your name';
		}

		if ( empty( $_POST[ 'contact_email' ] ) || !is_email( $_POST[ 'contact_email' ] ) ) {
			$this->errors[ 'contact_email' ] = 'Please enter a valid email address';
		}

		if ( empty( $_POST[ 'contact_phone' ] ) ) {
			$this->errors[ 'contact_phone' ] = 'Please enter your phone number';
		}

		if ( empty( $_POST[ 'contact_enquiry_type' ] ) ) {
			$this->errors[ 'contact_enquiry_type' ] = 'Please choose an enquiry type';
		}

		if( !$newsletter && empty( $_POST['contact_message' ] ) ) {
			$this->errors[ 'contact_message' ] = 'Please enter a message';
		}

		if ( empty( $this->errors ) ) {
			return true;
		} else {
			return false;
		}
	}
}
