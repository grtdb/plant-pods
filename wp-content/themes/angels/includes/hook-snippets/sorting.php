<?php // Sorting

function db_sorting_wrapper() {

	$category = get_queried_object();

	if ( !is_search() && is_shop() ) { return; }

	?><section id="filters-nav" class="part-product_sort">
		<div class="container">
			<div id="filter-posts">
				<span class="fa fa-th"></span>
				<p>Filter Products</p>
			</div>
			<?php do_action( 'db_sorting_wrapper_inside' ); ?>
			<div id="sort-posts">
				<p>Sort By</p><?php
}

function db_sorting_wrapper_close() {

	$category = get_queried_object();

	if ( !is_search() && is_shop() ) { return; }

			?></div>
		</div>
	</section><?php
}

function db_sorting_dropdown() {
	?><section id="filter-posts-dropdown">
		<div class="container">
			<?php if ( is_active_sidebar( 'product_filter_dropdown' ) ) { ?>
				<div id="product-filter-widget" class="primary-sidebar widget-area" role="complementary">
					<?php dynamic_sidebar( 'product_filter_dropdown' ); ?>
				</div>
			<?php } ?>
		</div>
	</section><?php
}