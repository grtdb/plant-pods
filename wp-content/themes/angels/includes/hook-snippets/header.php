<?php

function db_mobile_buttons() {
	?><div id="mobile-header-buttons" class="d-block d-md-none">
		<button id="mob-nav" class="mob-button">
			<div class="nav-icon">
				<span></span>
				<span></span>
				<span></span>
				<span></span>
			</div>
		</button>
	</div><?php
}

// Header cart text
function db_header_cart() {
	?><div id="header-cart" class="d-none d-sm-block">
		<a id="header-cart-text" class="cart-contents" href="<?php echo esc_url( WC()->cart->get_cart_url() ); ?>">
			<span class="amount"><?php echo wp_kses_data( WC()->cart->get_cart_subtotal() ); ?></span>
			<span class="count"><?php echo wp_kses_data( sprintf( _n( '%d item', '%d items', WC()->cart->get_cart_contents_count(), 'storefront' ), WC()->cart->get_cart_contents_count() ) );?></span>
		</a><?php
		if ( !is_cart() && !is_checkout() ) {
			?><div id="header-cart-dropdown"><?php
				the_widget( 'WC_Widget_Cart', 'title=' );
			?></div><?php
		}
	?></div><?php
}
