<?php // Prduct Page

function db_product_page_start() {
	?><div class="container">
		<div class="row">
			<div class="col-md-5 product-image-wrap"><?php
}

function db_product_wrap_start() {
			?></div>
			<div class="col-md-7 product-detail-wrap"><?php
}

function db_product_wrap_end() {
			?></div>
		</div>
	</div><?php
}

function db_product_code() {
	global $product;

	if ( wc_product_sku_enabled() && ( $product->get_sku() || $product->is_type( 'variable' ) ) ) { ?>
		<p id="product-code"><span class="sku" itemprop="sku"><?php echo ( $sku = $product->get_sku() ) ? $sku : __( 'N/A', 'woocommerce' ); ?></span></p>
	<?php }
}

function db_product_summary_start() {
	?><div id="product-details"><?php
}

function db_product_summary_mid() {
	?></div>

	<div id="product-cart"><?php
}

function db_product_summary_end() {
	?></div><?php
}

function db_product_price_wrap_start() {
	?><div id="product-price"><?php
}

function db_product_price_wrap_end() {
	?></div><?php
}

function db_product_tabs() {
	?><div id="product-tabs" class="tabs">
		<ul>
			<li data-tab="details" class="active">Details</li>
			<li data-tab="delivery">Delivery</li>
			<li data-tab="returns">Returns</li>
		</ul>
		<div>
			<div data-name="details" class="active">
				<div class="dyn-content"><?php
					the_content();
				?></div>
			</div>
			<div data-name="delivery">
				<div class="dyn-content">
					<?php echo html_entity_decode( apply_filters( 'the_content', get_option( 'delivery_information' ) ) ); ?>
				</div>
			</div>
			<div data-name="returns">
				<div class="dyn-content">
					<?php echo html_entity_decode( apply_filters( 'the_content', get_option( 'returns_information' ) ) ); ?>
				</div>
			</div>
		</div>
	</div><?php
}

function db_product_reviews() {
	$press_reviews = get_field( 'reviews' );

	if ( $press_reviews ) {
		?><section id="product-reviews">
			<div class="container">
				<h3 class="title">Press Reviews</h3><?php

				foreach ( $press_reviews as $review ) {
					?><div class="review">
						<div class="dyn-content">
							<p><?php echo $review['content']; ?></p>
						</div>
						<div class="author">
							<img data-src="<?php echo $review['author_image']; ?>" alt="<?php echo $review['author']; ?>" />
						</div>
					</div><?php
				}
		?></div>
		</section><?php
	}
}


function db_product_relateds() {
	global $product;

	$products = $product->get_upsells();

	if ( $products ) {
		dbHelper::get_part( 'product-carousel', array( 'id' => 'related-products', 'title' => 'Related Products', 'items' => $products ) );
	}
}

function db_product_recents() {
	$products = db_get_recently_viewed_products();

	if ( $products ) {
		dbHelper::get_part( 'product-carousel', array( 'id' => 'recently-viewed-products', 'title' => 'Recently Viewed Products', 'items' => $products ) );
	}
}