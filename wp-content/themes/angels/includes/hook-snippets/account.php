<?php

function db_account_nav_start() {
	?><div id="account-sidebar"><?php
}

function db_account_nav_end() {
	?></div><?php
}

function db_account_content_start() {
	?><div id="account-content"><?php
}

function db_account_content_end() {
	?></div><?php
}