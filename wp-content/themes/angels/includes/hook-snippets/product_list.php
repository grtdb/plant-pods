<?php // Product List

// Product
function db_product_list_item_top() {
	?><div class="inner">
		<div class="image"><?php
}

function db_product_list_item_middle() {
		?></div>
		<div class="content"><?php
}

function db_product_list_item_bottom() {
		?></div>
	</div><?php
}

// Category
function db_category_list_item_top( $category ) {
	if ( !$image = wp_get_attachment_image_src( get_woocommerce_term_meta( $category->term_id, 'thumbnail_id' ), 'category_carousel' )[0] ) {
		$image = dbHelper::get_placeholder();
	}
	?><div class="image">
		<img src="<?= $image; ?>" alt="<?= $category->name; ?>" />
	</div><?php
}

function db_category_list_item_middle() {
	?><div class="content"><?php
}

function db_category_list_item_bottom( $category ) {
		/*if ( $excerpt = get_field( 'category_excerpt', $category ) ) {
			?><p><?= dbHelper::shorten_text( $excerpt ); ?></p><?php
		}*/
	?></div><?php
}

// Page Layout
function db_description_wrapper() {
	?><section class="part-description">
		<div class="container">
			<h1 class="title"><?php woocommerce_page_title(); ?></h1>
			<div class="dyn-content"><?php
}

function db_description_wrapper_close() {
			?></div>
		</div>
	</section><?php
}