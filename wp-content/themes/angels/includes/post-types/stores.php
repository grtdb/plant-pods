<?php

new dbStores();

class dbStores extends dbCustomPostType {

	public $post_type = 'stores';
	public $slug = 'stores';

	public function __construct() {
		// Post Type Details
		$this->name = 'Stores';
		$this->singular_name = 'Store';
		$this->menu_icon = 'dashicons-admin-multisite';

		// Post Type Arguments
		$this->args = array(
			'supports'      => array( 'title', 'thumbnail', 'editor', 'page-attributes' ),
			'rewrite'       => array( 'with_front' => true, 'slug' => $this->slug ),
			'menu_position' => 23,
		);

		// Build Post Type
		parent::__construct();

		// Related Image Sizes
		//add_image_size( '', 300, 300, true );

	}
}