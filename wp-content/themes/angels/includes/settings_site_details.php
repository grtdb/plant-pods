<?php

$site_settings = new dbOptionsPage( 'Site Details', 'site_options' );

$site_settings->add_section( 'site_details', array(
	'label' => 'Website Information',
	'fields' => array(
		'copyright_text' => array( 'Copyright Text', 'input', 'width:100%;' ),
	)
) );

$site_settings->add_section( 'api_keys', array(
	'label' => 'Keys & Codes',
	'fields' => array(
		'google_maps_api_key' => array( 'Google Maps API Key', 'input', 'width:100%;' ),
	)
) );

$social_fields = array();
foreach ( dbHelper::get_socials() as $social_key => $social_name ) {
	$social_fields[ $social_key . '_title' ] = array( $social_name . ' URL Title', 'input', 'width:60%' );
	$social_fields[ $social_key . '_url' ] = array( $social_name . ' URL', 'input', 'width:60%' );
}
$site_settings->add_section( 'social_media', array(
	'label' => 'Social Media',
	'fields' => $social_fields
) );