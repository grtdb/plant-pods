<?php

$delivery_settings = new dbOptionsPage( 'Product Information', 'product_info' );

$delivery_settings->add_section( 'product_delivery', array(
	'label' => '',
	'fields' => array(
		'delivery_information' => array( 'Delivery Information', 'editor', 'width:100%;' ),
		'returns_information' => array( 'Returns Information', 'editor', 'width:100%;' ),
	)
) );