<?php

// Check if category has children
function product_cat_has_children( $category ) {

	if ( isset( $category->term_id ) ) {
		$category_id = $category->term_id;
		$children = get_categories( array( 'child_of' => $category_id, 'taxonomy' => 'product_cat' ) );

		if ( !empty( $children ) ) {
			return true;
		}
	} return false;
}

// Edit woocommerce breadcrumbs
add_filter( 'woocommerce_breadcrumb_defaults', function( $defaults ) {
	return array(
		'delimiter' => '<span>&gt;</span>',
		'home' => false,
	);
} );

// Recently viewed products
function db_get_recently_viewed_products() {
	$viewed_products = ! empty( $_COOKIE['woocommerce_recently_viewed'] ) ? (array) explode( '|', $_COOKIE['woocommerce_recently_viewed'] ) : array();
	$viewed_products = array_filter( array_map( 'absint', $viewed_products ) );

	if ( empty( $viewed_products ) ) {
		return false;
	}

	return $viewed_products;
}

// Track recently viewed products
function db_track_recently_viewed_products() {
	if ( ! is_singular( 'product' ) ) {
		return;
	}

	global $post;

	if ( empty( $_COOKIE['woocommerce_recently_viewed'] ) )
		$viewed_products = array();
	else
		$viewed_products = (array) explode( '|', $_COOKIE['woocommerce_recently_viewed'] );

	if ( ! in_array( $post->ID, $viewed_products ) ) {
		$viewed_products[] = $post->ID;
	}

	if ( sizeof( $viewed_products ) > 15 ) {
		array_shift( $viewed_products );
	}

	// Store for session only
	wc_setcookie( 'woocommerce_recently_viewed', implode( '|', $viewed_products ) );
}

// Add except to categories
if ( function_exists( "register_field_group" ) ) {
	register_field_group(array (
		'id' => 'acf_product-category-excerpt',
		'title' => 'Product Category excerpt',
		'fields' => array (
			array (
				'key' => 'field_57e3a21fbc085',
				'label' => 'Excerpt Text',
				'name' => 'category_excerpt',
				'type' => 'text',
				'instructions' => 'Enter a short excerpt for the category list item.',
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'formatting' => 'none',
				'maxlength' => '',
			),
		),
		'location' => array (
			array (
				array (
					'param' => 'ef_taxonomy',
					'operator' => '==',
					'value' => 'product_cat',
					'order_no' => 0,
					'group_no' => 0,
				),
			),
		),
		'options' => array (
			'position' => 'acf_after_title',
			'layout' => 'no_box',
			'hide_on_screen' => array (
			),
		),
		'menu_order' => 0,
	));
}
