<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8" />
		<meta name="robots" content="noindex,nofollow" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0" />
		<title><?php wp_title( ' | ', true, 'right' ); ?></title>
		<?php wp_head(); ?>
		<link rel="shortcut icon" href="/fav.png" />
		<script src="https://use.typekit.net/jjm0lps.js"></script>
		<script>try{Typekit.load({ async: true });}catch(e){}</script>
		<!--[if lte IE 8]>
			<script src="//html5shiv.googlecode.com/svn/trunk/html5.js"></script>
			<script src="<?php //echo get_stylesheet_directory_uri(); ?>/assets/js/respond.min.js"></script>
		<![endif]-->
	</head>
	<body>
		<!--[if lte IE 9]>
			<div class="es-browser-upgrade"><p>You are viewing this website on an outdated browser which may be insecure and may hinder your experience and limit functionality. Please consider upgrading. <a href="http://browsehappy.com" target="_blank">Click here to update</a></p></div>
		<![endif]-->
		<?php dbHelper::get_part( 'mob-nav' ); ?>

		<div id="site-wrapper">
			<header>
				<section id="header" class="clearfix">
					<div class="container">
						<div class="row">
							<div class="col-md-12">
								<?php do_action( 'db_location_header_mob' ); ?>
								<div id="header-contact">
									<a href="tel:<?= get_option( 'contact_phone' ); ?>">Call Us: <?= get_option( 'contact_phone' ); ?></a>
								</div>
								<a id="header-logo" href="/">
									<img src="<?= get_template_directory_uri(); ?>/assets/img/logo-new.png" alt="<?php bloginfo( 'name' ); ?>" />
								</a>
								<?php do_action( 'db_location_header_cart' ); ?>
								<a href="<?= esc_url( WC()->cart->get_cart_url() ); ?>"><span class="fa fa-shopping-cart"></span></a>
							</div>
						</div>
					</div>
				</section>

				<section id="navigation">
					<div class="container">
						<div class="row">
							<div class="col-md-12">
								<nav id="navigation-left">
									<?php wp_nav_menu( array( 'theme_location' => 'header_nav_left', 'container' => false ) ); ?>
								</nav>
								<nav id="navigation-right">
									<?php wp_nav_menu( array( 'theme_location' => 'header_nav_right', 'container' => false ) ); ?>
								</nav>
								<span id="search-button" class="fa fa-search"></span>
							</div>
						</div>
					</div>
				</section>

				<?php dbHelper::get_part( 'drop-search' ); ?>
			</header>