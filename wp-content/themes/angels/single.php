<?php /* Standard Single Template */
get_header(); the_post(); $page_id = get_the_ID(); ?>

<div id="main" class="single-standard">

	<section id="page-main">
		<div class="container">
			<div class="post-head">
				<p class="title"><?php the_title(); ?></p>
				<p class="date">Posted <?php the_time( 'd.m.Y' ); ?></p>
				<a id="back" class="p-bold" href="<?php echo get_permalink( get_option( 'page_for_posts' ) ); ?>"><span></span>Back</a>
			</div>
			<?php $image = dbHelper::get_featured_url( get_the_ID(), 'blog_detail' );
			if ( $image ) { ?>
				<div class="image" style="background-image:url('<?php echo $image; ?>');"></div>
			<?php } ?>
			<div class="dyn-content"><?php the_content(); ?></div>
			<div class="share-links" data-social-title="<?php the_title(); ?>" data-social-text="<?php echo get_the_excerpt(); ?>">
				<p class="p-bold">Share</p>
				<ul>
					<li class="twitter" data-social="twitter"><i class="fa fa-twitter" aria-hidden="true"></i></li>
					<li class="facebook" data-social="facebook"><i class="fa fa-facebook" aria-hidden="true"></i></li>
					<li class="linkedin" data-social="linkedin"><i class="fa fa-linkedin" aria-hidden="true"></i></li>
					<?php /*<li class="googleplus" data-social="googleplus"><i class="fa fa-google-plus" aria-hidden="true"></i></li>*/ ?>
				</ul>
			</div>
			<hr/>
			<?php
				$prev_post = get_previous_post();
				if ( !empty( $prev_post ) ) { ?>
					<a class="prev p-bold" href="<?php echo get_permalink( $prev_post->ID );?>"><span></span>Previous</a>
				<?php }

				$next_post = get_next_post();
				if ( !empty( $next_post ) ) { ?>
					<a class="next p-bold" href="<?php echo get_permalink( $next_post->ID ); ?>">Next<span></span></a>
				<?php } ?>

		</div>
	</section>

</div>

<?php get_footer();