/*global module:false*/
module.exports = function(grunt) {

	grunt.initConfig({
		pkg: grunt.file.readJSON('package.json'),
		sass: {
			dist: {
				files: [{
					expand: true,
					cwd: 'assets_src/sass',
					src: ['*.scss'],
					dest: 'assets/css/src',
					ext: '.css'
				}]
			}
		},
		uglify: {
			options: {
				banner: '<%= banner %>'
			},
			dist: {
				files: [{
					expand: true,
					cwd: 'assets_src/js',
					src: ['**/*.js'],
					dest: 'assets/js/src/',
					ext: '.js'
				}]
			}
		},
		concat: {
			options: {
				separator: ';'
			},
			dist: {
				files: {
					'assets/js/dist/core.min.js': [
						'assets/js/src/**/*.js'
					],
					'assets/js/dist/lib.min.js': [
						'node_modules/jquery/dist/jquery.min.js',
						'assets_src/vendor/jquery/plugins/**/*.js',
						'node_modules/bootstrap/dist/js/bootstrap.js'
					]

				}
			}
		},
		concat_css: {
			options: {},
			dist: {
				files: {
					'assets/css/_build/core.css': [
						'assets/css/src/*.css'
					],
					'assets/css/src/vendor.css': [
						'assets_src/vendor/**/*.css'
					]
				}
			}
		},
		cssmin: {
			minify: {
				files: {
					'assets/css/dist/core.min.css': [
						'assets/css/_build/core.css'
					],
					'assets/css/dist/vendor.min.css': [
						'assets/css/src/vendor.css'
					]
				}
			}
		},
		jshint: {
			options: {
				curly: true,
				eqeqeq: true,
				immed: true,
				latedef: true,
				newcap: true,
				noarg: true,
				sub: true,
				undef: true,
				unused: true,
				boss: true,
				eqnull: true,
				globals: {
					jQuery: true
				}
			},
			gruntfile: {
				src: 'Gruntfile.js'
			}
		},
		watch: {
			javascript: {
				files: [
					'assets_src/*/**/*.js'
				],
				tasks: ['uglify', 'jshint', 'concat'],
				options : {
					spawn: false
				}
			},
			sass: {
				files: [
					'assets_src/*/**/*.scss'
				],
				tasks: ['sass'],
				options : {
					spawn: false
				}
			},
			css: {
				files: [
					'assets_src/*/**/*.css'
				],
				tasks: ['concurrent:target', 'concat_css'],
				options : {
					spawn: false
				}
			},
			gruntfile: {
				files: '<%= jshint.gruntfile.src %>',
				tasks: ['jshint:gruntfile']
			}
		},
		concurrent: {
			target: ['uglify', 'cssmin']
		},
		imagemin: {
			dynamic: {
				files: [{
				  expand: true,
				  cwd: 'assets_src/img',
				  src: ['**/*.{png,jpg,gif}'],
				  dest: 'assets/img/'
				}]
			}
		}
	});

	// These plugins provide necessary tasks
	grunt.loadNpmTasks('grunt-contrib-concat');
	grunt.loadNpmTasks('grunt-concat-css');
	grunt.loadNpmTasks('grunt-contrib-uglify');
	grunt.loadNpmTasks('grunt-contrib-jshint');
	grunt.loadNpmTasks('grunt-contrib-watch');
	grunt.loadNpmTasks('grunt-contrib-cssmin');
	grunt.loadNpmTasks('grunt-concurrent');
	grunt.loadNpmTasks('grunt-contrib-sass');
	grunt.loadNpmTasks('grunt-contrib-imagemin');

	// Default task
	grunt.registerTask('default', ['sass', 'jshint', 'concurrent:target', 'concat', 'concat_css', 'imagemin']);
	grunt.registerTask('production', ['sass', 'uglify', 'jshint', 'concurrent:target', 'concat', 'concat_css', 'cssmin', 'imagemin']);
};
